#!/usr/bin/env bash

################################################################################
# Check if all required systemd units are installed in the system
################################################################################

UNITS=(
abrtd.service
accounts-daemon.service
basic.target
display-manager.service
getty.target
graphical.target
kdm.service
multi-user.target
network-online.target
network.target
rescue.service
rescue.target
rtkit-daemon.service
sshd.service
switcheroo-control.service
systemd-update-utmp-runlevel.service
udisks2.service
usbguard.service
)

CHECK_DIRS=(
/etc/systemd
/usr/lib/systemd
)

UNITS_LEN=${#UNITS[@]}

# DON'T TOUCH UNDER THIS LINE! #################################################

echo

units_found=0
for f in "${UNITS[@]}"
  do
  found=0
  for d in "${CHECK_DIRS[@]}"
    do
    if [[ $found == 0 ]];
    then
      cd $d
      lines=$(find . -name $f | wc -l)
      if [[ $lines != 0 ]]
      then
          found=1
          units_found=$((units_found+1))
      fi
    fi
  done
  if [[ $found == 0 ]];
  then
    echo "Missing: $f"
  fi
done

echo
echo "Summary:"
echo "  - # units to check: $UNITS_LEN"
echo "  - # units found: $units_found"

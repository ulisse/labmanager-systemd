#!/bin/bash
# LabManager: systemd units & scripts installer v2.0
GIT_URL=https://gitlab.di.unipmn.it
GIT_BASE_DIR=ulisse/labmanager-systemd/raw
GIT_BRANCH=master

ESAME_DIR=/opt/esame

BIN_FILES=(
"desktop-wallpaper" "esamesync-esame.sh" "firefox-home" "get-config" "iptables-rules" "kdm-autologin" "kioskmode" "mk-homedir" "nfs" "usb" "virtualbox"
)

SYSTEMD_FILES=(
"esame-desktop-wallpaper.service" "esame-firefox-home.service" "esame-get-config-boot.service" "esame-get-config.service" "esame-iptables-rules.service"
"esame-kdm-autologin.service" "esame-kioskmode.service" "esame-mk-homedir.service" "esame-nfs.service" "esame-usb.service" "esame-virtualbox.service"
"esamesync-esame.service" "esame-get-config.timer" "esame-configurator.target" "esame.target" "esamekiosk.target" "esameshow.target" "esame-multi-user.target"
)

SYSCONFIG_FILES=(
"labmanager"
"esamesync-esame"
)

OLD_FILES=(
"/usr/libexec/labmanager"
"/etc/systemd/system/esame.target.wants"
"/etc/systemd/system/esamekiosk.target.wants"
"/etc/systemd/system/esameshow.target.wants"
"/etc/systemd/system/esame-configurator.target.wants"
"/etc/systemd/system/graphical.target.wants/esame-get-config.timer"
"/etc/systemd/system/esame-desktop-wallpaper.service"
"/etc/systemd/system/esame-firefox-home.service"
"/etc/systemd/system/esame-get-config-boot.service"
"/etc/systemd/system/esame-get-config.service"
"/etc/systemd/system/esame-iptables-rules.service"
"/etc/systemd/system/esame-kdm-autologin.service"
"/etc/systemd/system/esame-kioskmode.service"
"/etc/systemd/system/esame-mk-homedir.service"
"/etc/systemd/system/esame-nfs.service"
"/etc/systemd/system/esame-usb.service"
"/etc/systemd/system/esame-virtualbox.service"
"/etc/systemd/system/esamesync-esame.service"
"/etc/systemd/system/esame-get-config.timer"
"/etc/systemd/system/esame-configurator.target"
"/etc/systemd/system/esame.target"
"/etc/systemd/system/esamekiosk.target"
"/etc/systemd/system/esameshow.target"
)

# DON'T TOUCH UNDER THIS LINE! #################################################

# Create a user
myuseradd() {
  if [ "z$1" == "z" ]; then
    echo "myuseradd: No user specified! Nothing to do."
    return
  fi
  R_USER="$1"
  R_UID=""
  if [ "z$2" != "z" ]; then
    R_UID="-u $2"
  fi
  R_GID="-U"
  if [ "z$3" != "z" ]; then
    # We need to create the group before creating the user
    R_GID="-g $3"
    groupadd ${R_GID} -r ${R_USER}
  fi
  useradd -b /local -m ${R_UID} ${R_GID} -r ${R_USER}
  if [ $? -ne 0 ]; then
    echo "Error creating user ${R_USER}"
  else
    echo "User ${R_USER} was created"
  fi
}

function removeFiles {
    local files=( "$@" )

    for f in "${files[@]}"
    do
        rm -rf $f
        echo "Removed file: $f"
    done
}

function downloadFiles {
    local base_url="$1"
    shift
    local files=( "$@" )

    i=1
    for f in "${files[@]}"
    do
        echo -e "\n$i/${#files[@]} - $base_url/$f"
        curl -# --fail -O "$base_url/$f"
        if [[ $? != 0 ]]
        then
            echo "curl exited with error code != 0, installation aborted."
            exit 1
        fi
        i=$(expr $i + 1)
    done
}

echo -e "\nLabManager: systemd units & scripts installer"

if [ -z "$1" ]; then
    echo -e "\nNo branch/tag specified, using branch 'master'."
    echo -e "Please call '$0 <branch/tag>' to use a different source.\n"
else
    GIT_BRANCH="$1"
    echo -e "\nUsing branch/tag '$GIT_BRANCH'.\n"
fi

# v1.0 has a different set of files and install procedure, print warning and abort installation
if [[ "$GIT_BRANCH" == "v1.0" ]]
then
    echo -e "\e[1mWARNING!\e[0m"
    echo "Branch 'v1.0' has a different installation procedure, please get"
    echo -e "the correct installation script here:\n"
    echo -e "$GIT_URL/$GIT_BASE_DIR/v1.0\n"
    exit 1
fi

# remove obsolete files
echo -e "\e[1mRemoving obsolete files...\e[0m"
removeFiles ${OLD_FILES[@]}
echo
echo

# Create all 'esame' users, with homes under '/local'
echo -e "\e[1mAdding users and their home directories under /local...\e[0m"

mkdir -p /local
mkdir -p ${ESAME_DIR}

#useradd -b /local -m -u 968 -U -g 968 -r    esamesync
myuseradd esamesync 451 451
myuseradd esame
myuseradd moodle
myuseradd rdesktop
chown esamesync:esamesync ${ESAME_DIR}

mkdir -p /usr/libexec/labmanager

echo
echo

# script per i servizi systemd
echo -e "\e[1mDownloading systemd script files...\e[0m"
cd /usr/libexec/labmanager
echo "Creating symlink bta-no-usbpen -> /bin/sleep..."
ln -s -f /bin/sleep bta-no-usbpen
echo
downloadFiles "$GIT_URL/$GIT_BASE_DIR/$GIT_BRANCH/bin" ${BIN_FILES[@]}
chmod +x *
echo
echo

# sysconfig
# If the admin has created/modified the files, we will not overwrite them.
echo -e "\e[1mDownloading sysconfig files...\e[0m"
cd /etc/sysconfig
for conffile in ${SYSCONFIG_FILES[@]}; do
    if [ -e "${conffile}" ]; then
        echo "Not overwriting existing file ${conffile}, skipping it."
    else
        downloadFiles "$GIT_URL/$GIT_BASE_DIR/$GIT_BRANCH/sysconfig" "${conffile}"
   fi
done
echo
echo

# servizi systemd
echo -e "\e[1mDownloading systemd unit files...\e[0m"
cd /etc/systemd/system
downloadFiles "$GIT_URL/$GIT_BASE_DIR/$GIT_BRANCH/systemd" ${SYSTEMD_FILES[@]}
echo
echo

# following links should not be needed, specifying services as "Wants" directives should suffice

# create symlinks for target esame, esamekiosk and esameshow
mkdir esame.target.wants
cd esame.target.wants
ln -s /usr/lib/systemd/system/accounts-daemon.service .
ln -s /usr/lib/systemd/system/rtkit-daemon.service .
ln -s /usr/lib/systemd/system/switcheroo-control.service .
ln -s /usr/lib/systemd/system/systemd-update-utmp-runlevel.service .
ln -s /usr/lib/systemd/system/udisks2.service .
ln -s /usr/lib/systemd/system/veyon.service .
ln -s /etc/systemd/system/enable-wol@.service enable-wol@eth0.service
cd ..
mkdir esamekiosk.target.wants
cd esamekiosk.target.wants
ln -s /usr/lib/systemd/system/accounts-daemon.service .
ln -s /usr/lib/systemd/system/rtkit-daemon.service .
ln -s /usr/lib/systemd/system/switcheroo-control.service .
ln -s /usr/lib/systemd/system/systemd-update-utmp-runlevel.service .
ln -s /usr/lib/systemd/system/udisks2.service .
ln -s /usr/lib/systemd/system/veyon.service .
ln -s /etc/systemd/system/enable-wol@.service enable-wol@eth0.service
cd ..
mkdir esameshow.target.wants
cd esameshow.target.wants
ln -s /usr/lib/systemd/system/accounts-daemon.service .
ln -s /usr/lib/systemd/system/rtkit-daemon.service .
ln -s /usr/lib/systemd/system/switcheroo-control.service .
ln -s /usr/lib/systemd/system/systemd-update-utmp-runlevel.service .
ln -s /usr/lib/systemd/system/udisks2.service .
ln -s /usr/lib/systemd/system/veyon.service .
ln -s /etc/systemd/system/enable-wol@.service enable-wol@eth0.service
cd ..

echo -e "\e[1mReload systemd...\e[0m"
systemctl daemon-reload
echo
echo

echo -e "\e[1mEnable systemd services...\e[0m"
systemctl enable esame-*.service
systemctl enable esamesync-esame.service
systemctl enable esame-get-config.timer
systemctl start esame-get-config.timer
echo
echo

echo -e "\e[1mDisable unwanted systemd services...\e[0m"
systemctl disable esame-usb.service
echo
echo

echo -e "\e[1mChange default target to esame-configurator.target...\e[0m"
systemctl set-default esame-configurator.target
echo
echo

echo -e "\e[1mAll done!\e[0m"

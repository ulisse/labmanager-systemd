#!/usr/bin/env -S bash

FROM=1
TO=48
LAB=edu-al.unipmmn.it
SRC_FILE=
DEST_PATH=

if [ ! -z "$5" ]; then
    FROM=$1
    TO=$2
    LAB=$3
    SRC_FILE=$4
    DEST_PATH=$5
else
    cat <<EOF

Usage:
    $0 <from> <to> <lab domain> <file> <dest_path>

Example:
    $0 1 20 lminf.unipmn.it bin/get-config /usr/libexec/labmanager

EOF
    exit 0
fi

for m in `seq -f "%02g" ${FROM} ${TO}`; do
    echo $m;
    ./deploy-file.sh lab${m}.${LAB} $SRC_FILE $DEST_PATH root
done

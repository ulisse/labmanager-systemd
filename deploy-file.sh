#!/usr/bin/env -S bash

USER=root
MACHINE=
SRC_FILE=
DEST_PATH=


if [ -z "$3" ]; then
  echo "Usage: ./deploy-file.sh <machine> <src_file> <dest_path> [user]"
  exit 1
fi

MACHINE=$1
SRC_FILE=$2
DEST_PATH=$3

if [ -n "$4" ]; then
  USER="$4"
fi

# backup old file
T=`date +"%Y-%m-%d_%H%M"`
FILENAME=$(basename $SRC_FILE)

ssh ${USER}@${MACHINE} "cd $DEST_PATH; cp --preserve $FILENAME ${FILENAME}_$T"

rsync -rlptvz -e ssh $SRC_FILE ${USER}@${MACHINE}:$DEST_PATH
